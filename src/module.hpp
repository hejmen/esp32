#pragma once

#include <Vector.h>
#include "configserver.hpp"

#define ELEMENT_COUNT_MAX 8

class Module {
	public:
		virtual void setup() = 0;
		virtual void loop() = 0;
		virtual void render(int offsetX, int offsetY) = 0;
		virtual Config* getConfig() = 0;
		virtual String getValue(String name) { return "NaN"; };
};

class ModuleConfig : public Config {
	public:
		ModuleConfig();
		virtual String getName() override;
		virtual String getSchema() override;
		virtual void setConfig(JsonDocument* doc) override;
		virtual void setDefault() override;

		String mqtt_prefix;
		Module* modules_array[ELEMENT_COUNT_MAX];
		Vector<Module*> modules;

		String modules_title_array[ELEMENT_COUNT_MAX];
		Vector<String> modules_titles;
};

extern ModuleConfig modulecfg;

enum Modules {
	MODULE_TH = 1
};

class ModuleManager {
	public:
		ModuleManager();
		void initModules();
		void configureModules(ConfigServer* configServer);
		void setupAll();
		void loopAll();
		void render();
		void publish(String topic, String message);
};

extern ModuleManager moduleManager;
