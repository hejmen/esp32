#pragma once

#include "configserver.hpp"


class WifiConfig : public Config {
	public:
		virtual String getName() override;
		virtual String getSchema() override;
		virtual void setConfig(JsonDocument* doc) override;
		virtual void setDefault() override;
		char mAPName[8];
		char mAPPass[8];
		char mWiFiName[16];
		char mWiFiPass[16];
};

class WifiManager {
	public:
		WifiManager();
		void setup();
		void loop();
		String ip;
		int wifistate;
	private:
		int counter;
};

extern WifiManager wifimgr;
extern WifiConfig wificfg;
