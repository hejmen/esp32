#ifndef NO_MODULE_TH

#include "module_th.hpp"
#include "display.hpp"

#define DHTTYPE DHT22

MTHConfig::MTHConfig(String config): config(config){
}

String MTHConfig::getName() {
	return config;
}

String MTHConfig::getSchema() {
	return "/m_mth.schema.json";
}

void MTHConfig::setDefault() {
	pin = 1;
	mqtt = "";
}

void MTHConfig::setConfig(JsonDocument* doc) {
	JsonObject obj = doc->as<JsonObject>();
	pin = obj[String("pin")];
	String m = obj[String("mqtt")];
	Serial.println("pin " + String(pin));
	mqtt = m;
}

MTH::MTH(String config): mthconfig(MTHConfig(config)){
	Serial.println("MTH CONFIG");
	temperature = 1.0 / 0.0;
	humidity = 1.0 / 0.0;
	configServer.readConfiguration(&mthconfig);
}

Config* MTH::getConfig() {
	return &mthconfig;
}

void MTH::setup() {
	//pinMode(DHTPin, INPUT);
	dht.setup(mthconfig.pin);
}

void MTH::loop() {
	temperature = dht.getTemperature();
	humidity = dht.getHumidity();
	if (isnan(temperature) || isnan(humidity))
		return;

	moduleManager.publish(mthconfig.mqtt + "/temp", String(temperature));
	moduleManager.publish(mthconfig.mqtt + "/humi", String(humidity));
	Serial.println(String(temperature));
}

String MTH::getValue(String name) {
	if(name == NAME_TEMP)
		return String(temperature);
	if(name == NAME_HUMI)
		return String(humidity);
	return "NaN";
}

void MTH::render(int offsetX, int offsetY) {
	display.setTextAlignment(TEXT_ALIGN_LEFT);
	display.setFont(ArialMT_Plain_24);
	display.drawString(offsetX + 0, offsetY + 0, getValue(NAME_TEMP) + "°C");
	display.setFont(ArialMT_Plain_10);
	display.drawString(offsetX + 0, offsetY + 30, getValue(NAME_HUMI) + "%");
}

#endif
