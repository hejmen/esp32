#include "wifi.hpp"
#include "configserver.hpp"
#include <WiFi.h>

#define AP_WAIT_TIME 40
#define CONN_WAIT_TIME 40

WifiManager wifimgr;
WifiConfig wificfg;

const char* AP_N = "ESP_AP";

String WifiConfig::getName() {
	return "/wifi.sec.json";
}

String WifiConfig::getSchema() {
	return "/wifi.schema.json";
}

void WifiConfig::setDefault() {
	strlcpy(mAPName, AP_N, sizeof(mAPName));
}

void WifiConfig::setConfig(JsonDocument* doc) {
	JsonObject obj = doc->as<JsonObject>();
	strlcpy(mAPName, obj[String("apn")] | AP_N, sizeof(mAPName));
	strlcpy(mAPPass, obj[String("app")] | "", sizeof(mAPPass));
	strlcpy(mWiFiName, obj[String("wfn")] | "", sizeof(mWiFiName));
	strlcpy(mWiFiPass, obj[String("wfp")] | "", sizeof(mWiFiPass));
}

WifiManager::WifiManager() {
	wifistate = 0;
	counter = 0;
}

void WifiManager::setup() {
	configServer.readConfiguration(&wificfg);
}

void WifiManager::loop() {
	switch(wifistate) {
	case 0: // Init state
		//Start AP
		Serial.println("Wifi: AP MODE");
		Serial.println(wificfg.mAPName);
		WiFi.softAP(wificfg.mAPName, wificfg.mAPPass);
		wifistate = 1;
		counter = AP_WAIT_TIME;
		ip = WiFi.softAPIP().toString();
		Serial.println(WiFi.softAPIP());
		break;
	case 1: // AP Mode
		counter--;
		// Timeout and noone is connected = try to connect
		if (counter < 0 && WiFi.softAPgetStationNum() == 0 ) {
			wifistate = 2;
			WiFi.softAPdisconnect();
			WiFi.begin(wificfg.mWiFiName, wificfg.mWiFiPass);
			counter = CONN_WAIT_TIME;
			Serial.println("Wifi: CONNECTING");
		}
		break;
	case 2: // Connection in progress
		counter--;
		if (WiFi.status() == WL_CONNECTED) {
			wifistate = 3;
			Serial.println("Wifi: CONNECTED");
			ip = WiFi.localIP().toString();
			Serial.println(WiFi.localIP());
		} else if(counter < 0) {
			wifistate = 0;
			Serial.println("Wifi: TIMEOUT");
		}
		break;
	case 3:	// Connection lost
		if (WiFi.status() != WL_CONNECTED) {
			wifistate = 0;
			Serial.println("Wifi: CONNECTION LOST");
		}
		break;
	default:
		break;
	}
}
