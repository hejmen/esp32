#pragma once
#ifndef NO_MODULE_TH

#include "configserver.hpp"
#include "module.hpp"
#include "DHT.h"

#define NAME_TEMP "T"
#define NAME_HUMI "H"

class MTHConfig : public Config {
	public:
		MTHConfig(String config);
		virtual String getName() override;
		virtual String getSchema() override;
		virtual void setConfig(JsonDocument* doc) override;
		virtual void setDefault() override;
		int pin;
		String mqtt;
	private:
		String config;
};

class MTH: public Module {
	public:
		MTH(String config);
		void setup();
		void loop();
		String getValue(String name);
		void render(int offsetX, int offsetY);
		Config* getConfig();
	private:
		MTHConfig mthconfig;
		DHT dht;
		float temperature;
		float humidity;
};

#endif
