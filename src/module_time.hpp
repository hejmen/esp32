#pragma once
#ifndef NO_MODULE_TIME

#include "configserver.hpp"
#include "module.hpp"

class MTimeConfig : public Config {
	public:
		MTimeConfig(String config);
		virtual String getName() override;
		virtual String getSchema() override;
		virtual void setConfig(JsonDocument* doc) override;
		virtual void setDefault() override;
		int timezone;
	private:
		String config;
};

class MTime: public Module {
	public:
		MTime(String config);
		void setup();
		void loop();
		String getValue(String name);
		void render(int offsetX, int offsetY);
		Config* getConfig();
	private:
		MTimeConfig mtimeconfig;
};

#endif
