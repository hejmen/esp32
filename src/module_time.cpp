#ifndef NO_MODULE_TIME

#include "module_time.hpp"
#include "display.hpp"
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <WiFi.h>
#include "font.hpp"

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pl.pool.ntp.org", 3600, 60000);

MTimeConfig::MTimeConfig(String config): config(config){
}

String MTimeConfig::getName() {
	return config;
}

String MTimeConfig::getSchema() {
	return "/m_mtime.schema.json";
}

void MTimeConfig::setDefault() {
	timezone = 0;
}

void MTimeConfig::setConfig(JsonDocument* doc) {
	JsonObject obj = doc->as<JsonObject>();
	timezone = obj[String("timezone")];
}

MTime::MTime(String config): mtimeconfig(MTimeConfig(config)){
	configServer.readConfiguration(&mtimeconfig);
}

int time_wifi = false;

Config* MTime::getConfig() {
	return &mtimeconfig;
}

void MTime::setup() {
}

void MTime::loop() {
	if(WiFi.status() == WL_CONNECTED && !time_wifi) {
		timeClient.begin();
		time_wifi = true;
	}
	if (time_wifi)
		timeClient.update();
}

String MTime::getValue(String name) {
	return "NaN";
}

char buffer[5];

void MTime::render(int offsetX, int offsetY) {
	display.setTextAlignment(TEXT_ALIGN_LEFT);
	display.setFont(DSEG14_Classic_Regular_30);
	sprintf(buffer, "%02d:%02d", timeClient.getHours() + mtimeconfig.timezone, timeClient.getMinutes());
	display.drawString(offsetX, offsetY, buffer);
}

#endif
