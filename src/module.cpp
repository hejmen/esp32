#include "module.hpp"
#include "configserver.hpp"
#include "module_th.hpp"
#include "module_time.hpp"
#include "display.hpp"
#include <MQTT.h>
#include <WiFi.h>
#include "wifi.hpp"

ModuleManager moduleManager;
MQTTClient client;
WiFiClient net;

ModuleConfig modulecfg;

ModuleConfig::ModuleConfig() {
	modules.setStorage(modules_array);
	modules_titles.setStorage(modules_title_array);
}

String ModuleConfig::getName() {
	return "/module.json";
}

String ModuleConfig::getSchema() {
	return "/module.schema.json";
}

void ModuleConfig::setDefault() {
	mqtt_prefix = "";
}

void ModuleConfig::setConfig(JsonDocument* doc) {
	JsonObject obj = doc->as<JsonObject>();
	String mqtt = obj[String("mqtt")];
	mqtt_prefix = mqtt;
	JsonArray arr = obj[String("modules")];
	for (auto value : arr) {
		String name = value[String("name")];
		String title = value[String("title")];
		String module = value[String("module")];
		Serial.println(name);
		Serial.println(module);
		if (module.equals("MTH")) {
			modules.push_back(new MTH(name));
			modules_titles.push_back(title);
		} else if (module.equals("MTime")) {
			modules.push_back(new MTime(name));
			modules_titles.push_back(title);
		}
	}
}

ModuleManager::ModuleManager() {
}

void messageReceived(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);
}

void ModuleManager::initModules() {
	configServer.server.on("/api/get", HTTP_GET, [this] (AsyncWebServerRequest *request) {
		if(!request->hasParam(PARAM_NAME) || !request->hasParam(PARAM_GET))
			return request->send(400, "text/json", "{\"error\": 1}");

		int name = request->getParam(PARAM_NAME)->value().toInt();
		String get = request->getParam(PARAM_GET)->value();
		Serial.println(String(name) + " : " + get);


		if (modulecfg.modules.size() <= name && name > 0)
			return request->send(400, "text/json", "{\"error\": 2}");

		request->send(200, "text/json", "{\"value\": \"" + modulecfg.modules[name]->getValue(get) + "\"}");
	});

	configServer.server.on("/api/modules", HTTP_GET, [this] (AsyncWebServerRequest *request) {
		String message = "{";
		for (int i = 0; i < modulecfg.modules.size() ; i++) {
			if (i != 0)
				message += ", ";

			message += "\"" + modulecfg.modules_titles[i] + "\":\"" + modulecfg.modules[i]->getConfig()->getName() + "\"";
		}
		message += "}";

		request->send(200, "text/json", message);
	});

	setup_display();
	configServer.readConfiguration(&modulecfg);
	Serial.println("Init Modules");
}

void ModuleManager::configureModules(ConfigServer* configServer) {
	client.begin("192.168.1.105", net);
	client.onMessage(messageReceived);
}

void ModuleManager::setupAll() {
	for(int i = 0;i< modulecfg.modules.size(); i++) {
		modulecfg.modules[i]->setup();
	}
}


void ModuleManager::publish(String topic, String message) {
	client.publish(modulecfg.mqtt_prefix + topic, message);
}

void ModuleManager::loopAll() {
	if(WiFi.status() == WL_CONNECTED) {
		if (!client.connected()) {
			client.connect("arduino", "user", "haslo");
		}
	}

	Serial.println("LoopAll");

	if(client.connected()) {
		client.loop();
	}

	Serial.println(modulecfg.modules.size());
	for(int i = 0;i< modulecfg.modules.size(); i++) {
		Serial.println(String(i));
		modulecfg.modules[i]->loop();
	}
}

int a = 0;
int pp = 0;

#define SCREEN_HEIGHT 96

void ModuleManager::render() {
	if (a %SCREEN_HEIGHT == 0 && pp < 200) {
		pp++;
		if (pp > 2)
			return;
	}
	else {
		pp = 0;
		a++;
	}

	display.clear();
	display.setColor(WHITE);


	int moduleNb = 0;
	for(int i = 0;i< modulecfg.modules.size(); i++) {
		int offset = 15 + (a + SCREEN_HEIGHT * i) % (SCREEN_HEIGHT * modulecfg.modules.size()) - SCREEN_HEIGHT;
		if (offset < SCREEN_HEIGHT && offset > 0)
			moduleNb = i;
		modulecfg.modules[i]->render(0, offset);
	}

	if (pp != 0) {
		// Draw bar
		display.drawLine(0, 15, 128, 15);

		// Draw title
		display.setFont(ArialMT_Plain_10);
		display.drawString(10, 0, modulecfg.modules_titles[moduleNb]);

		if (moduleNb == 0) {
			display.setTextAlignment(TEXT_ALIGN_RIGHT);
			String info = "";
			if (wifimgr.wifistate == 1 ) {
				info += "AP ";
			} else if (wifimgr.wifistate == 2 ) {
				info += "C..";
			} else if (wifimgr.wifistate == 3 ) {
				info += "C  ";
			}
			info += wifimgr.ip;
			display.drawString(120, 0, info);
		}
	}
	display.display();
}
