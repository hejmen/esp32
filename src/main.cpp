#include "configserver.hpp"
#include "module.hpp"
#include <WiFi.h>

#include "wifi.hpp"

unsigned long startTime;
unsigned int p = 0;

void setup()
{
	Serial.begin(115200);
	startTime = millis();
	delay(1000);

	// Initialize configuration
	configServer.init();
	wifimgr.setup();

	// Initialize modules
	moduleManager.initModules();
	moduleManager.configureModules(&configServer);
	moduleManager.setupAll();
}

void loop()
{
	delay(9);
	// Increment every 20ms
	if (millis() - startTime >= 20) {
		startTime = millis();

		configServer.loop();
		// Loop configuration every second
		if (p%50 == 0) {
			wifimgr.loop();
		}
		if (p%500 == 0) {
			moduleManager.loopAll();
		}
		// Ping rendering task
		if (p % 2 == 0) {
			moduleManager.render();
		}
		p++;
	}
}
