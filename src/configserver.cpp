#include "configserver.hpp"
#include "SPIFFS.h"
#include <WiFi.h>

ConfigServer configServer;
IPAddress ip;

const char* PARAM_NAME = "name";
const char* PARAM_TEXT = "text";
const char* PARAM_GET = "get";

ConfigServer::ConfigServer(): server(80) {
	configs.setStorage(configs_array);
}

void ConfigServer::init() {
	// Initialize SPIFFS
	if(!SPIFFS.begin(true)){
		Serial.println("An Error has occurred while mounting SPIFFS");
		return;
	}
	initWeb();
}

void ConfigServer::initWeb() {
	server.serveStatic("/img", SPIFFS, "/web/img");
	server.serveStatic("/css", SPIFFS, "/web/css");
	server.serveStatic("/js", SPIFFS, "/web/js");

	// Route for root / web page
	server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
		request->send(SPIFFS, "/web/index.html", String(), false);
	});

	server.on("/api/configs", HTTP_GET, [&] (AsyncWebServerRequest *request) {
		String message = "{";
		for (int i = 0; i < configs.size() ; i++) {
			if (i != 0)
				message += ", ";

			message += "\"" + configs[i]->getName() + "\":\"" + configs[i]->getSchema() + "\"";
		}
		message += "}";

		request->send(200, "text/json", message);
	});

	server.on("/api/save", HTTP_PUT, [] (AsyncWebServerRequest *request) {
		if(!request->hasParam(PARAM_NAME) || !request->hasParam(PARAM_TEXT))
			return request->send(400, "text/json", "{\"error\": 1}");

		Serial.println(request->getParam(PARAM_NAME)->value());
		Serial.println(request->getParam(PARAM_TEXT)->value());
		File file = SPIFFS.open("/config" + request->getParam(PARAM_NAME)->value(), FILE_WRITE);
		if(file.print(request->getParam(PARAM_TEXT)->value())) {
		}
		file.close();
		Serial.println("SAVE");

		request->send(200, "text/plain", "OK");
	});

	server.on("/api/config", HTTP_GET, [] (AsyncWebServerRequest *request) {
		String name = "/config" + request->getParam(PARAM_NAME)->value();
		// Skip secure files
		if(name.indexOf(".sec.") > 0) {
			request->send(200, "text/json", "{}");
			return;
		}
		// Skip if not json
		if(name.indexOf(".json") < 0) {
			request->send(200, "text/json", "{}");
			return;
		}
		request->send(SPIFFS, name, String(), false);
	});

	server.on("/api/schema", HTTP_GET, [] (AsyncWebServerRequest *request) {
		String name = "/schema" + request->getParam(PARAM_NAME)->value();
		request->send(SPIFFS, name, String(), false);
	});
}

IPAddress emptyIP(0,0,0,0);

void ConfigServer::loop() {
	if(WiFi.localIP() == emptyIP) {
		if(WiFi.softAPIP() != ip) {
			server.begin();
			ip = WiFi.softAPIP();
			Serial.println(ip);
		}
	} else {
		if(WiFi.localIP() != ip) {
			server.begin();
			ip = WiFi.localIP();
			Serial.println(ip);
		}
	}
}

bool ConfigServer::readConfiguration(Config* config) {
	configs.push_back(config);

	File file = SPIFFS.open("/config" + config->getName(), "r");
	if (!file){
		Serial.println(config->getName() + "NOT FOUND");
		config->setDefault();
		return false;
	}

	//DynamicJsonDocument doc(512);
	StaticJsonDocument<512> doc;
	DeserializationError error = deserializeJson(doc, file);
	if (error) {
		config->setDefault();
		return false;
	}

	config->setConfig(&doc);
	return true;
}
