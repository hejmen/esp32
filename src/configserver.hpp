#pragma once

#include <Arduino.h>
#include <ArduinoJson.h>
#include <Vector.h>

#define CONFIGS_COUNT_MAX 10

#include "ESPAsyncWebServer.h"

class Config {
	public:
		virtual String getName();
		virtual String getSchema();
		virtual void setConfig(JsonDocument* doc);
		virtual void setDefault();
};

class ConfigServer {
	public:
		ConfigServer();
		void init();
		void loop();
		bool readConfiguration(Config *config);
		AsyncWebServer server;
	private:
		void initWeb();
		Config* configs_array[CONFIGS_COUNT_MAX];
		Vector<Config*> configs;
};

extern const char* PARAM_NAME;
extern const char* PARAM_TEXT;
extern const char* PARAM_GET;
extern ConfigServer configServer;
