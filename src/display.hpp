#pragma once
#ifndef NO_MODULE_DISPLAY

#include <Wire.h>
#include "SSD1306Wire.h"

void setup_display();
extern SSD1306Wire display;

#endif
