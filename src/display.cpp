#include "display.hpp"

SSD1306Wire display(0x3c, 5, 4);

void setup_display() {
	display.init();
	display.flipScreenVertically();
}
