let val = 0

module.exports = {
	outputDir: '../data/web',
	productionSourceMap: false,
	configureWebpack: {
		performance: { hints: false },
		output: {
			filename: 'js/web.js'
		},
		optimization: {
			splitChunks: false
		}
	},
	filenameHashing: false,
	pluginOptions: {
		"serve-api-mocks": {
			base: "/api",
			routes: [
				{
					method: "GET",
					path: "/configs",
					callback(req, res) {
						res.status(200)
						.send("{\"/wifi.sec.json\":\"/wifi.schema.json\", \"/module.json\":\"/module.schema.json\", \"/time.json\":\"/module_mtime.schema.json\", \"/temp.json\":\"/module_mth.schema.json\"}")
						.end();
					},
				},
				{
					method: "GET",
					path: "/modules",
					callback(req, res) {
						res.status(200)
						.send("{\"Time\":\"/time.json\", \"Room\":\"/temp.json\"}")
						.end();
					},
				},
				{
					method: "GET",
					path: "/schema",
					callback(req, res) {
						if (req.query.name == '/module_mth.schema.json') {
							res.status(200)
							.send("{\"get\": {\"T\": {\"name\": \"temperature\", \"metric\": \"°C\"},\"H\": {\"name\": \"humidity\", \"metric\": \"%\"}},\"config\": {\"pin\": {\"name\": \"Pin number\"},\"mqtt\": {\"name\": \"MQTT class\"}}}")
							.end();
						} else {
							res.status(200)
							.send("{}")
							.end();
						}
					},
				},
				{
					method: "GET",
					path: "/config",
					callback(req, res) {
						if (req.query.name == '/temp.json') {
							res.status(200)
							.send("{\"pin\": \"2\"}")
							.end();
						} else {
							res.status(200)
							.send("{}")
							.end();
						}
					},
				},
				{
					method: "GET",
					path: "/get",
					callback(req, res) {
						if (req.query.name == '1') {
							res.status(200)
							.send("{\"value\": \"" + val++ + "\"}")
							.end();
						} else {
							res.status(200)
							.send("{\"value\": \"NaN\"}")
							.end();
						}
					},
				}
			],
		},
	},
}
